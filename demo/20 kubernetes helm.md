# 20 Kubernetes Helm

* demo app in the cloud
  * frontend: http://demo.osiris-project.com/
  * backend: http://demo-api.osiris-project.com/
  * ops view: http://kube-ops-view.osiris-project.com/

* init cluster `./init-cluster.sh`

* token: `eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLWN6NXRrIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI1ZmVjZjc5OC03N2YxLTExZTktOWZmNC0wNjk2YTFmYTc5MzEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06YWRtaW4tdXNlciJ9.YFaAoR4y5pgaVaa3S8QzvBqN_Vh6ssQDIkBXQZL7wT_gP9sC4JMXxG13niu19KBXSbGBiN_J_KZHsnFK0Om51zBj3M3tofTpROxLGGwU2Koxn4mtXP4XCe5-LXpdskao3wwi8TyBX5U5imLQOcMWzp7_jLTEMm_4ixtpN3fv2bxTXS1_YrLHCLnP-mJBEtDLp-g28gSFFAarK9hWj-zzYzWqoMYd6DtM10rtB4TMHxLvRY27BWZHu0IMWFDYGSn3HsxkY_y_QGeZ0tQPj4CsdTCAYjYV12nMb_3qB919FvVtyrEd7A9-c0BYJP6zKd4rHrZmY8cIE4G5ND868EmhbQ`

* check <http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/>

* <https://helm.sh/>

* install tiller

```shell
# initialize tiller and wait for tiller deployment to be ready
helm --kubeconfig=config/goca-test-kubeconfig.yaml  init --service-account admin-user
kubectl --kubeconfig=config/goca-test-kubeconfig.yaml -n kube-system rollout status deployment/tiller-deploy
```

* install nginx with helm

```shell
# deploy nginx and wait for nginx deployment to be ready
helm --kubeconfig=config/goca-test-kubeconfig.yaml install stable/nginx-ingress --name nginx-ingress --set rbac.create=true --namespace kube-system
kubectl --kubeconfig=config/goca-test-kubeconfig.yaml -n kube-system rollout status deployment/nginx-ingress-controller
```

* install kube-ops-view and ingress on <http://kube-ops-view.goca-training.be>

```shell
# deploy kube-ops-view
kubectl --kubeconfig=config/goca-test-kubeconfig.yaml apply -f kube-ops-view/
```

* wait for the loadbalancer to be ready and get the ip: 138.68.122.197

* add lines to `C:\Windows\System32\drivers\etc\hosts`

```shell
	138.68.122.197  kube-ops-view.goca-training.be
```

* install nginx with helm

```shell
helm  --kubeconfig=config/goca-test-kubeconfig.yaml install --name demo-db-mongo --namespace demo --set image.tag=latest,usePassword=false,mongodbEnableIPv6=false,replicaSet.enabled=true,replicaSet.name=demo-db,persistence.enabled=true,persistence.storageClass=do-block-storage,replicaSet.replicas.secondary=3 stable/mongodb
```
