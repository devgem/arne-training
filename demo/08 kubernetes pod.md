# 08 Kubernetes pod

## Follow along

* create a yaml file with pod for the my-app container in `kube/my-app-pod.yaml`

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-app-pod
  namespace: my-app
  labels:
    app: my-app-pod
spec:
  containers:
    - name: my-app-container
      image: devgem/my-app:3.0
      env:
        - name: my_message
          value: "Hello from GOCA on Kubernetes"
      resources:
        limits:
          cpu: 1500m
          memory: 1024Mi
        requests:
          cpu: 50m
          memory: 50Mi
```

* install the pod in kubernetes

```shell
kubectl apply -f kube/my-app-pod.yaml
```

* browse to the the container via the kubectl proxy: <http://localhost:8001/api/v1/namespaces/default/pods/http:my-app-pod:/proxy/>

* check out the pods

```shell
kubectl get pods
```