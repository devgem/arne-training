# 16 Kubernetes scale and loadbalance

## Follow along

* open <http://demo.goca-training.be:31222/>
* an open <http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/overview?namespace=demo>

* adjust `kube/demo-backend.yaml` to replicas 3

* watch the "Read-request container history" in the frontend and redeploy the backend

```shell
kubectl apply -f kube/demo-backend.yaml
```
