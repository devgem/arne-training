# 01 Docker intro

## Follow along

* list all docker images on your local machine

```shell
docker image list
```

* pull an image from the "Docker Hub" registry to your local machine (see <https://hub.docker.com/_/nginx/>)

```shell
docker pull nginx
```

* list the image on your local machine

```shell
docker image list
```

* list all docker containers on your local machine

```shell
docker container list -a
```

* run a container on your local machine and expose port 80 of the container to port 5000 on your local machine

```shell
docker run -p 5000:80 nginx
```

* surf to <http://localhost:5000>
* you will see the container logs to the console
* press `Ctrl+C` to stop the console running
* the container keeps on running
  * refresh browser
  * list all running containers
* logs can still be consulted

```shell
docker logs <CONTAINERID_OR_CONTAINERNAME>
```

* try to remove the container

```shell
docker container rm <CONTAINERID_OR_CONTAINERNAME>
```

* did not work since it's running, so stop the container

```shell
docker container stop <CONTAINERID_OR_CONTAINERNAME>
```

* list all docker containers

```shell
docker container list -a
```

* remove the container

```shell
docker container rm <CONTAINERID_OR_CONTAINERNAME>
```

* surf again to <http://localhost:5000>
* run the same container
  * with the latest version
  * as a daemon
  * with a significant name

```shell
docker run -p 5000:80 --name nginx -d nginx:1.16.0
```

* run a container on the same machine
  * with the previous nginx version
  * on a different port
  * with a different name

```shell
docker run -p 5001:80 --name nginx-prev -d nginx:1.15.12
```

* list both images on your local machine

```shell
docker image list
```

* list both docker containers on your local machine

```shell
docker container list -a
```

* surf to <http://localhost:5000>
* and surf to <http://localhost:5001>
* an follow the logs of both containers in different consoles

```shell
docker logs -f nginx
```

```shell
docker logs -f nginx-prev
```

* shortest way to shut down and remove all containers...
* display all container ids

```shell
docker container list -aq
```

* and feed the ids to a forces docker remove command

```shell
docker container rm --force $(docker container list -aq)
```

* list all containers again

```shell
docker container list -a
```
