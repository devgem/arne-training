# 03 Docker linking containers

## Demo

* run a sql server 2017 container, see <https://hub.docker.com/_/microsoft-mssql-server>

```shell
docker run --name sqlserver2017 -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=goca' -e 'MSSQL_PID=Express' -d mcr.microsoft.com/mssql/server:2017-latest-ubuntu
```

* link the sqlpad container to the sql server

```shell
docker run --name sqlpad --link sqlserver2017:sqlserver -d -p 3000:3000 sqlpad/sqlpad
```

* check out the containers

```shell
docker container list -a
```

* check why sql server did not start

```shell
docker logs sqlserver2017
```

* remove the the containers and recreate them properly

```shell
docker container rm $(docker container list -aq)
docker run --name sqlserver2017 -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Goca2019!' -e 'MSSQL_PID=Express' -d mcr.microsoft.com/mssql/server:2017-latest-ubuntu
docker run --name sqlpad --link sqlserver2017:sqlserver -d -p 3000:3000 sqlpad/sqlpad
```

* check out the containers

```shell
docker container list -a
```

* check out the sqlpad site on <http://localhost:3000>
* create a connection to the sqlserver container
* do a query

```sql
SELECT * FROM master.sys.databases;
```

* create a database

```sql
CREATE DATABASE goca;
```

* create a table with some data

```sql
USE goca;
CREATE TABLE Persons (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255)
);
INSERT INTO Persons (PersonID, LastName, FirstName)
VALUES
    (1, 'Bill', 'Gates'),
    (2, 'Steve', 'Jobs'),
    (3, 'Elon', 'Musk'),
    (4, 'Jeff', 'Bezos'),
    (5, 'Gavin', 'Belson');
```

* query the table

```sql
USE goca;
SELECT * FROM Persons;
```

* stop and remove the sql server 2017 container

```shell
docker container rm --force sqlserver2017
```

* recreate the sql server 2017 container

```shell
docker run --name sqlserver2017 -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Goca2019!' -e 'MSSQL_PID=Express' -d mcr.microsoft.com/mssql/server:2017-latest-ubuntu
```

* do a query

```sql
SELECT * FROM master.sys.databases;
```
