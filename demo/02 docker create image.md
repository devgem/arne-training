# 02 Docker create image

## Demo

* run an nginx container

```shell
docker run -d --name nginx -p 5000:80 nginx:1.16.0
```

* bash into it

```shell
docker exec -it nginx bash
```

* check out the filesystem inside the docker container

```shell
ls
cd /usr/share/nginx/html
ls
cat index.html
```

* leave the container

```shell
exit
```

* check out the default page <http://localhost:5000>
* create a new `index.html` file in `my-app` folder

```html
<html>
    <head>
        <title>Hello</title>
    </head>
    <body>
        <h1>Hello from my app!</h1>
    </body>
</html>
```

* copy it to the container

```shell
docker cp my-app/index.html nginx:/usr/share/nginx/html
```

* check out the new page <http://localhost:5000>
* commit the container to an new image

```shell
docker commit nginx  devgem/my-app:1.0
```

* remove all containers

```shell
docker container rm --force $(docker container list -aq)
```

* start an old container on 5000 and a new one one 5001

```shell
docker run -d --name nginx -p 5000:80 nginx:1.16.0
docker run -d --name my-app -p 5001:80 devgem/my-app:1.0
```

* check the pages on <http://localhost:5000> and <http://localhost:5001>
* show devgem account in Docker Hub (login jeankedotcom)
* push the image to Docker Hub

```shell
docker push devgem/my-app:1.0
```

* show the pushed image and tags on Docker Hub
* update the message in `index.html` to show `version 2.0`
* create a docker image as you should with the `Dockerfile`

```shell
docker build -t devgem/my-app:2.0 .
```

* run the new container on port 5002 with a different name

```shell
docker run -d --name my-app2 -p 5002:80 devgem/my-app:2.0
```

* show it <http://localhost:5002>
* push the new image

```shell
docker push devgem/my-app:2.0
```

* show it on <https://microbadger.com/>
* if needed refresh microbadger via the webhook<https://hooks.microbadger.com/images/devgem/my-app/o2nDk0NcO22xgy7E__rfOwAxDPI=>

## Follow along

* run the new container on your local machine

```shell
docker run -d --name my-app2 -p 5002:80 devgem/my-app:2.0
```

## Demo

* add a variable MY_MESSAGE to `index.html` for `version 3`
* check out `env-setup.sh`
* append to `DockerFile`

```shell
COPY env-setup.sh /usr/local/bin
RUN chmod u+x /usr/local/bin/env-setup.sh

CMD ["/usr/local/bin/env-setup.sh"]
```

* build version 3.0

```shell
docker build -t devgem/my-app:3.0 .
```

* run version 3.0 on your local machine with yet another name on yet another poer

```shell
docker run -d --name my-app3 -p 5003:80 -e my_message="Hello from GOCA" devgem/my-app:3.0
```

* check <http://localhost:5003/>
* run another with another message

```shell
docker run -d --name my-app4 -p 5004:80 -e my_message="Hi from Sint-Agathe-Berchem!" devgem/my-app:3.0
```

* push the new image

```shell
docker push devgem/my-app:3.0
```

## Follow along

* run the new container with environment variable on your local machine

```shell
docker run -d --name my-app4 -p 5004:80 -e my_message="Whatever message you want" devgem/my-app:3.0
```
