# 09 Kubernetes service

## Follow along

* create a yaml file with the service for the my-app container in `kube/my-app-service.yaml`

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-app-service
spec:
  selector:
    app: my-app-pod
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

* install the service in kubernetes

```shell
kubectl apply -f kube/my-app-service.yaml
```

* browse to the the container via the kubectl proxy: <http://localhost:8001/api/v1/namespaces/default/services/http:my-app-service:/proxy/>

* check out the services

```shell
kubectl get services
```

* change type to NodePort

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-app-service
spec:
  selector:
    app: my-app-pod
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  type: NodePort
```

* redeploy the service in kubernetes

```shell
kubectl apply -f kube/my-app-service.yaml
```

* check the port

```shell
kubectl get services
```

* access the service via the port on localhost <http://localhost:32382>
