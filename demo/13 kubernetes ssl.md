# 13 Kubernetes ssl

## Follow along

* generate a self signed certificate for *.goca-training.be (install openssl if needed: <https://www.sslcertificaten.nl/support/OpenSSL/OpenSSL_-_Installatie_onder_Windows>)

```shell
mkdir cert
openssl req -subj "/C=BE/ST=Brussel/L=Brussel/O=GOCA/OU=IT/CN=*.goca-training.be" -newkey rsa:2048 -nodes -keyout cert/goca-trainnig.be.key -x509 -days 365 -out cert/goca-trainnig.be.crt
```

* create a secret in Kubernetes with the certificate as input

```shell
kubectl create secret tls tls-for-star.goca.training.be --key cert/goca-trainnig.be.key --cert cert/goca-trainnig.be.crt
```

* you can also create a yaml file for the secret in e.g. `cert/tls.yaml` and apply it with kubectl but it is safer to not store the yaml in sourcecontrol and keep your .crt and .key files safe

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: tls-for-star.goca.training.be
  namespace: default
type: kubernetes.io/tls
data:
  tls.crt: <FILL_IN_YOUR_CRT_BASE64_ENCODED>
  tls.key: <FILL_IN_YOUR_KEY_BASE64_ENCODED>
```

* add the `annotations` and `tls` sections to `kube/my-app-ingress.yaml`

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: my-app-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.org/ssl-services: "my-app-service"
spec:
  tls:
    - hosts:
        - myapp.goca-training.be
      secretName: tls-for-star.goca.training.be
  rules:
    - host: myapp.goca-training.be
      http:
        paths:
          - backend:
              serviceName: my-app-service
              servicePort: 80
```

* Redeploy the ingress

```shell
kubectl apply -f kube/my-app-ingress.yaml
```

* go to the http url and see it redirect: <http://myapp.goca-training.be:31222>

* but since we are using a different port in the local test environment we need to specify the new port: <https://myapp.goca-training.be:31167>