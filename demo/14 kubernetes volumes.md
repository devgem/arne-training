# 14 Kubernetes volumes

## Follow along

* create a folder for the volume on the host

```shell
mkdir c:/nginx-volume
```

* make folder accessible

* create a yaml file for the persistent volume in `kube/nginx-pv.yaml`

```yaml
kind: PersistentVolume
apiVersion: v1
metadata:
  name: nginx-pv
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/c/nginx-volume"
```

* create a yaml file for the persistent volume claim in `kube/nginx-pvc.yaml`

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: nginx-pvc
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
```

* create a yaml file for the pod in `kube/nginx-pod.yaml`

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
  labels:
    app: nginx-pod
spec:
  containers:
    - name: nginx-container
      image: nginx:1.16.0
      volumeMounts:
        - mountPath: /usr/share/nginx/html
          name: nginx-volume
  volumes:
    - name: nginx-volume
      persistentVolumeClaim:
        claimName: nginx-pvc
```

* deploy it all

```shell
kubectl apply -f kube/nginx-pv.yaml
kubectl apply -f kube/nginx-pvc.yaml
kubectl apply -f kube/nginx-pod.yaml
```

* make sure the proxy is running

```shell
kubectl proxy
```

* browse to the the container via the kubectl proxy: <http://localhost:8001/api/v1/namespaces/default/pods/http:nginx-pod:/proxy/>

* create an index.html file in `c:/nginx-volume`

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Hi</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>

<body>
    <h1>Hello from nignx on Kubernetes</h1>
</body>

</html>
```

* refresh the browser: <http://localhost:8001/api/v1/namespaces/default/pods/http:nginx-pod:/proxy/>

* change something in `c:/nginx-volume` and refresh the browser: <http://localhost:8001/api/v1/namespaces/default/pods/http:nginx-pod:/proxy/>

* delete it all

```shell
kubectl delete -f kube/nginx-pod.yaml
kubectl delete -f kube/nginx-pvc.yaml
kubectl delete -f kube/nginx-pv.yaml
```

* add an image to `c:/nginx-volume`

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Hi</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>

<body>
    <h1>Hello from nignx on Kubernetes</h1>
    <img src="images/kube.png" />
</body>

</html>
```

* redeploy it all

```shell
kubectl apply -f kube/nginx-pv.yaml
kubectl apply -f kube/nginx-pvc.yaml
kubectl apply -f kube/nginx-pod.yaml
```

* refresh the browser: <http://localhost:8001/api/v1/namespaces/default/pods/http:nginx-pod:/proxy/>
* check the logs: <http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/pod/default/nginx-pod?namespace=default>
* refresh the browser again
* recheck the logs again
