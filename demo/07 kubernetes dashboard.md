# 07 Kubernetes dashboard

* install dashboard and start proxy

```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
kubectl proxy
```

* create admin user and get token

```shell
kubectl apply -f kube/admin-user.yaml
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
```

* token `eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLWsyMjZkIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJmNzE0MjQzMy03N2ZiLTExZTktYTJmZC0wMDE1NWQwMGQ1YTAiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06YWRtaW4tdXNlciJ9.mrKZ7IAVvcl6nEsvAkr2oHhBj7c4LqGZv-uEIgrQ-4HLIrxOIkJvTUJSnu2FuathyzdVi1F-0Vyn6Ras6_l61SjbCgAkMeTxx6Au1W-Zz0JZVMp8hYAA0cdKpw4R3cwHDr5Kx6zSyh0sC1DRBncRZU_vqba64ww88zFazIqpbVVJhOJVJnpm_i89cEF-hrbTlM6uzyZvARfvYE8KkCOPTa-4SfL22Ne9FcNB_gal3j3EYUhgwsWE6YK7hdciw7IgprbYauAEpknV8BgJLA5qDzL2P0rajWIisKxcp4WEFFIvhjcsiMoKq0rb_KBrbtKIIFanpJjRla-dF91ujG5CmA`
* open <http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/>

* install heapster to get cpu and memory graphs anf waint until its installed

```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
kubectl apply -f kube/heapster-controller.yaml
kubectl -n kube-system rollout status deployment/heapster
```

* restart dashboard pod from dashboard and refresh it to get the graphs