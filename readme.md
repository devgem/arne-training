# Arne Kubernetes training

## Day 1

###  Slides

* [Powerpoint](https://gitlab.com/devgem/arne-training/blob/master/slides/arne-kubernetes.pptx)

### Demos and exercises

#### Docker
1. [Intro](https://gitlab.com/devgem/arne-training/blob/master/demo/01%20docker%20intro.md)
2. [Creste image](https://gitlab.com/devgem/arne-training/blob/master/demo/02%20docker%20create%20image.md)
3. [Linking containers](https://gitlab.com/devgem/arne-training/blob/master/demo/03%20docker%20linking%20containers.md)
4. [Volumes](https://gitlab.com/devgem/arne-training/blob/master/demo/04%20docker%20volumes.md)
5. [Multiple docker containers](https://gitlab.com/devgem/arne-training/blob/master/demo/05%20multiple%20docker%20containers.md)